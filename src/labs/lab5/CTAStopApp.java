/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 03
 * date: Friday 04 Mar 9:00 am
 * program: CTAStopApp
 * description: This program shows how do we integrate CTAStation to build a CTAStopApp.
 */
package labs.lab5;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

public class CTAStopApp {
    public static void main(String[] args) {
        CTAStation[] stations = readFile("./CTAStops.csv");

        menu(stations);
    }

    private static void menu(CTAStation[] stations) {
        Scanner scanner = new Scanner(System.in);
        boolean running = true;
        while (running) {
            System.out.println("-----------------------------------------");
            System.out.println("Please choose the option you want to use:");
            System.out.println("1. Display Station Nmaes");
            System.out.println("2. Display Stations with/without Wheelchair");
            System.out.println("3. Display Nearest Station");
            System.out.println("4. Exit");
            System.out.println("-----------------------------------------");

            String input = scanner.nextLine();
            if (input.equals("")) {
                running = false;
                break;
            }
            switch (Integer.parseInt(input)) {
                case 1: {
                    displayStationNames(stations);
                    break;
                }
                case 2: {
                    displayByWheelchair(scanner, stations);
                    break;
                }
                case 3: {
                    displayNearest(scanner, stations);
                    break;
                }
                case 4: {
                    exit();
                    running = false;
                    break;
                }
            }
        }

        scanner.close();
    }

    private static void displayStationNames(CTAStation[] stations) {
        for (int i = 0; i < stations.length; i++) {
            System.out.println(stations[i].getName());
        }
    }

    private static void displayByWheelchair(Scanner scanner, CTAStation[] stations) {
        boolean retry = true;
        String yesOrNo = "";
        while (retry) {
            System.out.println("Please input your selection: y or n");

            yesOrNo = scanner.nextLine();
            if (yesOrNo.equals("y") || yesOrNo.equals("n"))
                retry = false;
        }

        boolean haveWheelchair = yesOrNo.equals("y");

        boolean found = false;
        for (int i = 0; i < stations.length; i++) {
            if (stations[i].hasWheelchar() == haveWheelchair) {
                System.out.println(stations[i]);
                found = true;
            }
        }
        if (!found) {
            System.out.println("no stations are found");
        }
    }

    private static void displayNearest(Scanner scanner, CTAStation[] stations) {
        System.out.println("Please input your location (lng, lat)");
        double lng = scanner.nextDouble();
        double lat = scanner.nextDouble();

        GeoLocation location = new GeoLocation(lng, lat);

        double dist = Double.MAX_VALUE;
        CTAStation nearesStation = stations[0];
        for (int i = 0; i < stations.length; i++) {
            double realDist = stations[i].calcDistance(location);
            if (realDist < dist) {
                dist = realDist;
                nearesStation = stations[i];
            }
        }
        System.out.println(nearesStation);
    }

    private static void exit() {
    }

    private static CTAStation[] readFile(String filename) {
        File f = new File(filename);
        CTAStation[] stations = new CTAStation[100];
        int counter = -1;
        Scanner scanner;
        try {
            scanner = new Scanner(f);
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                if (counter == -1) {
                    counter++;
                    continue;
                }

                String[] parts = line.split(",");
                CTAStation station = new CTAStation(
                        parts[0],
                        Double.parseDouble(parts[1]),
                        Double.parseDouble(parts[2]),
                        parts[3],
                        parts[4].equals("TRUE") ? true : false,
                        parts[5].equals("TRUE") ? true : false);
                stations[counter] = station;
                counter++;
            }
        } catch (Exception e) {
            System.out.println("Read file failed");
        }
        return Arrays.copyOfRange(stations, 0, counter);
    }

}

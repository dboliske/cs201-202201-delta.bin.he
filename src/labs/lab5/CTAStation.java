/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 02
 * date: Friday 04 Mar 9:00 am
 * program: CTAStation
 * description: This program CTAStation inherits from GeoLocation.
 */
package labs.lab5;

public class CTAStation extends GeoLocation{
    private String name;
    private String location;
    private boolean wheelchair;
    private boolean open;

    public CTAStation() {}

    public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
        super(lat, lng);
        this.location = location;
        this.name = name;
        this.wheelchair = wheelchair;
        this.open = open;
    }

    public String getName() {
        return this.name;
    }

    public String getLocation() {
        return this.location;
    }

    public boolean hasWheelchar() {
        return this.wheelchair;
    }

    public boolean isOpen() {
        return this.open;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String toString() {
        return String.format("%s-%s:(hasWheelchair:%s, isOpen: %s)", this.location, this.name, this.hasWheelchar() ? "yes" : "false", this.isOpen() ? "yes" : "false");
    }

    public boolean equals(CTAStation station) {
        return this.name.equals(station.getName()) && this.location.equals(station.getLocation());
    }
}

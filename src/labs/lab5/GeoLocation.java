/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 01
 * date: Friday 04 Mar 9:00 am
 * program: GeoLocation
 * description: This program define GeoLocation class.
 * grade.
 */
package labs.lab5;

public class GeoLocation {
    private double lat;
    private double lng;

    public GeoLocation() {
        this.lat = 0.0;
        this.lng = 0.0;
    }

    public GeoLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return this.lat;
    }

    public double getLng() {
        return this.lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String toString() {
        return String.format("(%.2f, %.2f)", this.lat, this.lng);
    }

    public boolean validLat(double lat) {
        return lat >= -90.0 && lat <= 90.0;
    }

    public boolean validLng(double lng) {
        return lng >= -180 && lng <= 180;
    }

    public boolean equals(GeoLocation location) {
        double exp = 1e-6;
        return Math.abs(this.lat - location.lat) < exp && Math.abs(this.lng - location.lng) < exp;
    }

    // we privide such a primitive method which can be used both signautures.
    public double getDistance(double lat1, double lat2, double lng1, double lng2) {
        return Math.sqrt(Math.pow(lat1,  lat2) + Math.pow(lng - lng2, 2));
    }

    public double calcDistance(GeoLocation location) {
        return this.getDistance(this.lat, this.lng, location.lat, location.lng);
    }

    public double calDistance(double lat, double lng) {
        return this.getDistance(this.lat, this.lng, lat, lng);
    }
}

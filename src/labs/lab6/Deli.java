/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 01
 * date: Thu 17 Mar 9:00 am
 * program: DeliApp
 * description: This Deli abstract our Deli's workflow.
 */
package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class Deli {
    private String name;
    private Scanner scanner;
    private ArrayList<Customer> customers;

    public Deli(String name) {
        this.name = name;
        this.scanner = new Scanner(System.in);
        this.customers = new ArrayList<>();
    }

    public Deli(String name, Scanner scanner) {
        this.name = name;
        this.scanner = scanner;
        this.customers = new ArrayList<>();
    }

    private final String welcomeMessage = String.format(
            "-------------------------\nWelcome to %s's deli, Please input one of those options:\n 1. Add new Customer to queue \n 2. Help Customer \n 3. Exit\n-------------------------\n",
            this.name);

    private void onNewCustomer() {
        System.out.println("Plase input next customer's name!");
        String customerName = this.scanner.nextLine();
        if (customerName.equals("")) {
            System.out.println("Please input valid customer's name!");
            return;
        }
        Customer customer = new Customer(customerName);
        this.customers.add(customer);
        System.out.println(
                String.format("Customer:%s is current in position:%d", customerName, this.customers.size()));
    }

    private void onHelpCustomer() {
        if (this.customers.size() > 0) {
            Customer frontCustomer = this.customers.get(0);
            this.customers.remove(0);
            System.out.println(String.format("Customer:%s is in serving", frontCustomer.getName()));
            return;
        }

        System.out.println("No available Customer for serving");
    }

    private void onExit() {

    }

    private void onInvalidOption() {
        System.out.println("Please input valid option number!");
    }

    public void run() {
        boolean running = true;

        while (running) {
            System.out.println(welcomeMessage);

            String command = this.scanner.nextLine();

            if (command.equals("1")) {
                this.onNewCustomer();
            } else if (command.equals("2")) {
                this.onHelpCustomer();
            } else if (command.equals("3")) {
                this.onExit();
                break;
            } else {
                this.onInvalidOption();
            }
        }
    }
}

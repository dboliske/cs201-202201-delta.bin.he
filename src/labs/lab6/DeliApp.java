/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 01
 * date: Thu 17 Mar 9:00 am
 * program: DeliApp
 * description: This DeliApp simulate the counter service
 */
package labs.lab6;

import java.util.Scanner;

public class DeliApp {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Deli deli = new Deli("hebin's deli", scanner);

        deli.run();

        scanner.close();
    }
}

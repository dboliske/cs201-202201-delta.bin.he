/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 01
 * date: Thu 17 Mar 9:00 am
 * program: Customer
 * description: This Customer define the abstraction for our customers.
 */
package labs.lab6;

public class Customer {
    private String name;
    private Double money;
    
    public Customer() {
        this.name = "";
        this.money = 0.0;
    }

    public Customer(String name) {
        this.name = name;
        this.money = 0.0;
    }

    public Customer(String name, double money) {
        this.name = name;
        this.money = money;
    }

    public String getName() {
        return this.name;
    }

    public double getMoney() {
        return this.money;
    }
}

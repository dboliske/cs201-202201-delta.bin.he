/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 2
 * date: Mon 4 Feb 9:00 am
 * program: DrawSqaure
 * discription: This programs shows how do we calculate average grade.
 */
package labs.lab2;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

class Calculator {
    public static double getAverage(ArrayList<Double> grades) {
        double sum = 0;
        for (int i = 0; i < grades.size(); i++)
            sum += grades.get(i);
        return sum / grades.size();
    }
}

public class GradeCalculator {
    public static void main(String[] args) {
        System.out.println("Please input your grades one by one, separate by blank, input -1 to stop inputing");
        Scanner scanner = new Scanner(System.in);

        ArrayList<Double> grades = new ArrayList<Double>();

        while (true) {
            double grade = scanner.nextDouble();
            if (grade == -1)
                break;
            grades.add(grade);
        }

        System.out.println(Calculator.getAverage(grades));

        scanner.close();

        // Test Calculator
        ArrayList<Double> testGrades = new ArrayList<>();
        int testCases = 1000;
        Random ra = new Random();
        double sum = 0;
        while (testCases > 0) {
            double grade = ra.nextDouble();
            testGrades.add(grade);
            sum += grade;
            testCases--;
        }
        double actualAverage = sum / (1000.0);
        double expectedAverage = Calculator.getAverage(testGrades);

        boolean testOk = true;
        StringBuffer testReport = new StringBuffer();
        if (Math.abs(actualAverage - expectedAverage) > 1e6) {
            testOk = false;
            testReport.append(String.format("expectedValue=%f,actualValue=%f", expectedAverage, actualAverage));
        }

        if (testOk) {
            System.out.println("Test Successfully");
        } else {
            System.out.println("Test Failed");
            System.out.println(testReport.toString());
        }
    }
}

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 3
 * date: Mon 4 Feb 9:00 am
 * program: Interactive
 * discription: This programs shows how do we interact with users.
 */
package labs.lab2;

import java.io.InputStream;
import java.util.Scanner;

class InteractiveAgent {
    // scanner is the input stream for this Agent.
    private Scanner scanner;
    // We use final strings to manage those options.
    private final String OPT_SAY_HELLO = "Say Hello";
    private final String OPT_ADDITION = "Addition";
    private final String OPT_MULTIPLICATION = "Multiplication";
    private final String OPT_EXIT = "Exit";

    public InteractiveAgent(InputStream in) {
        this.scanner = new Scanner(in);
    }

    private void sayHello() {
        String message = "Hello";
        System.out.println(message);
    }

    private void add(int a, int b) {
        int sum = a + b;
        System.out.println(sum);
    }

    private void multiply(int a, int b) {
        int product = a * b;
        System.out.println(product);
    }

    private void exit() {
        this.scanner.close();
    }

    private String usage() {
        StringBuffer usageMessage = new StringBuffer();
        usageMessage.append("Please input one of those options:\n");
        usageMessage.append("1. Say Hello: We will print hello to the console\n");
        usageMessage.append("2. Addition: You can input two numbers, we will print the sum of them\n");
        usageMessage.append("3. Multiplication: You can input two numbers, we will print the product of them\n");
        usageMessage.append("4. Exit: Exit the game\n");
        return usageMessage.toString();
    }



    public void run() {
        while (true) {
            System.out.println(this.usage());

            String cmd = this.scanner.nextLine();

            switch (cmd) {
                case OPT_SAY_HELLO: {
                    this.sayHello();
                    break;
                }
                case OPT_ADDITION: {
                    int a = this.scanner.nextInt();
                    int b = this.scanner.nextInt();
                    this.add(a, b);
                    break;
                }
                case OPT_MULTIPLICATION: {
                    int a = this.scanner.nextInt();
                    int b = this.scanner.nextInt();
                    this.multiply(a, b);
                    break;
                }
                case OPT_EXIT: {
                    this.exit();
                }
            }
        }
    }
}

public class Interactive {
    public static void main(String[] args) {
        InteractiveAgent agent = new InteractiveAgent(System.in);
        agent.run();
    }
}

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Mon 4 Feb 9:00 am
 * program: DrawSqaure
 * discription: This program draw a square based on user's input size.
 */
package labs.lab2;
import java.util.Scanner;

class SqaureDrawer {
    public static void draw(int size) {
        for(int i=0; i<size; i++) {
            for(int j=0; j<size; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}

public class DrawSquare {
    public static void main(String[] args) {
        System.out.println("Please input your square size: ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        SqaureDrawer.draw(size);
        scanner.close();
    }
}

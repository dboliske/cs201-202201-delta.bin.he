/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 6
 * date: Mon 31 Jan 9:00 am
 * program: Inch2Cm
 * discription: This program shows how do we transform from inch to cm.
 */
package labs.lab1;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

class Coverter {
    private static Map<String, Double> rule;
    static {
        rule = new HashMap<>();
        rule.put("inch->cm", 2.54);
    }

    public static double convert(String from, String to, double value) {
        return rule.get(from + "->" + to) * value;
    }
}

public class Inch2Cm {
    public static void main(String[] args) {
        System.out.println("Please input a value in inch: ");
        Scanner scanner = new Scanner(System.in);
        double inch = scanner.nextDouble();
        double cm = Coverter.convert("inch", "cm", inch);

        System.out.printf("You input the %.5f(inch), which is %.5f(cm).", inch, cm);

        // We use the similar approach to test our cases.
        // we randomly generated a lots of test cases.
        int testCases = 1000;
        boolean testOk = true;
        StringBuffer testReport = new StringBuffer();
        Random ra = new Random();
        while (testCases > 0) {
            double ic = ra.nextDouble();
            double expectedValue = ic * 2.54;
            double actualValue = Coverter.convert("inch", "cm", ic);
            if (Math.abs(expectedValue - actualValue) > 1e-3) {
                testOk = false;
                testReport.append(String.format("expectedValue=%.5f, actualValue=%.5f", expectedValue, actualValue));
                break;
            }
            testCases--;
        }

        if (testOk) {
            System.out.println("Test Succesfully");
        } else {
            System.out.println("Test Failed");
            System.out.println(testReport);
        }

        scanner.close();
    }
}

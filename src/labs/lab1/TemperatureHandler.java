/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 4
 * date: Mon 31 Jan 9:00 am
 * program: TemperatureHandler
 * discription: This program process the temperature in different formats.
 */
package labs.lab1;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.function.Function;

class Temperature {
    private Double value;
    private String unit;
    private static Map<String, Function<Double, Double>> rule;
    static {
        rule = new HashMap<>();
        rule.put("F->C", (f) -> {
            return (f - 32) * 5.0 / 9.0;
        });
        rule.put("C->F", (c) -> {
            return c * 9.0 / 5.0 + 32;
        });
    }

    public Temperature(double value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public Temperature convertTo(String unit) {
        if (this.unit.equals(unit))
            return this;
        String from = this.unit;
        String to = unit;

        return new Temperature(rule.get(from + "->" + to).apply(this.value), to);
    }

    public double getValue() {
        return this.value;
    }

    public String toString() {
        return String.format("%.5f(%s)", this.value, this.unit);
    }
}

public class TemperatureHandler {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input Fahrenheit, and display Celsius.
        System.out.println("Please input temperature in Fahrenheit: ");
        Double f = scanner.nextDouble();
        Temperature t1 = new Temperature(f, "F");
        System.out.println(t1.convertTo("C"));

        // Input Celsius, and display Fahrenheit
        System.out.println("Please input temperature in Celsius: ");
        Double c = scanner.nextDouble();
        Temperature t2 = new Temperature(c, "C");
        System.out.println(t2.convertTo("F"));
        scanner.close();

        // As we discard some precision, we need to regard those values are matched if
        // they are in a valid range.
        // Basically, we generated lots of random temperatures in Fahrenheit.
        // 1. then we use real formula to calculate the `expectedValue` for that value,
        // 2. we use that value to construct a `Temperature` class, and then convert it
        // to `Celsius` unit, we got the `actualValue` then
        // 3. Compare the `expectedValue` and `actualValue` in a reasonable range, then
        // we can check whether the program is correct or not.
        int testCases = 1000;
        boolean testOk = true;
        StringBuffer testReport = new StringBuffer();

        Random ra = new Random();
        while (testCases > 0) {
            double tValue = ra.nextDouble();
            // Expected value calculation
            double expectedValue = (tValue - 32) * 5 / 9.0;

            // First we construct Fahrenheit temperatures
            Temperature t = new Temperature(tValue, "F");
            double actualValue = t.convertTo("C").getValue();

            if (Math.abs(expectedValue - actualValue) > 1e-3) {
                testOk = false;
                testReport.append(String.format("expected=%.5f actual=%.5f", expectedValue, actualValue));
                break;
            }

            testCases--;
        }
        if (testOk)
            System.out.println("Test Successfully");
        else {
            System.out.println("Test Failed");
            System.out.println(testReport.toString());
        }
    }
}

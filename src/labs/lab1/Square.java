/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 5
 * date: Mon 31 Jan 9:00 am
 * program: Square
 * discription: This program process Box area calculation.
 */
package labs.lab1;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

class Box {
    private double length;
    private double width;
    private double depth;
    private String unit;
    private static Map<String, Double> rule;
    static {
        rule = new HashMap<>();
        rule.put("inch->feet", 0.0833333);
    }

    public Box(double length, double width, double depth, String unit) {
        this.length = length;
        this.width = width;
        this.depth = depth;
        this.unit = unit;
    }

    public Box convertTo(String unit) {
        if (this.unit == unit)
            return this;

        String from = this.unit;
        String to = unit;

        return new Box(this.length * rule.get(from + "->" + to), this.width * rule.get(from + "->" + to),
                this.depth * rule.get(from + "->" + to), to);
    }

    public double area() {
        return (this.depth * this.width + this.depth * this.length + this.length * this.width) * 2;
    }

}

public class Square {
    public static void main(String[] args) {
        System.out.println("Please input the length, width, depth one by one: ");
        Scanner scanner = new Scanner(System.in);
        double length = scanner.nextDouble();
        double width = scanner.nextDouble();
        double depth = scanner.nextDouble();
        Box box = new Box(length, width, depth, "inch");
        Box boxInFeet = box.convertTo("feet");
        System.out.println(String.format("We need %.4f(feet^2) to build this box", boxInFeet.area()));
        scanner.close();

        int testCases = 1000;
        boolean testOk = true;
        StringBuffer testReport = new StringBuffer();
        Random ra = new Random();
        while (testCases > 0) {
            double l = ra.nextDouble();
            double w = ra.nextDouble();
            double d = ra.nextDouble();

            double expectedValue = (l * w + l * d + w * d) * 2 * 0.0833333 * 0.0833333;
            Box b = new Box(l, w, d, "inch").convertTo("feet");
            double actualValue = b.area();

            if (Math.abs(expectedValue - actualValue) > 1e-3) {
                testOk = false;
                testReport.append(String.format("expectedValue=%.5f,actualValue=%.5f", expectedValue, actualValue));
                break;
            }

            testCases--;
        }

        if (testOk) {
            System.out.println("Test Succesfully");
        } else {
            System.out.println("Test Failed");
            System.out.println(testReport);
        }
    }
}

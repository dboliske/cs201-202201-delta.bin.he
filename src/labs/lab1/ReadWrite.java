/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Mon 31 Jan 9:00 am
 * program: ReadWrite
 * discription: This program shows how to read from command line and then output the input.
 */
package labs.lab1;

import java.util.Scanner;

public class ReadWrite {
    public static void main(String[] args) {
        System.out.println("Please input your name: ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        System.out.println("Your name is:" + name);

        System.out.println("Please input your age: ");
        int age = scanner.nextInt();
        System.out.println(age);
        System.out.println("Your age is:" + age);
        scanner.close();
    }
}

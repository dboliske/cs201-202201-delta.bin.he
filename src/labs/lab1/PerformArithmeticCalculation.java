/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 2
 * date: Mon 31 Jan 9:00 am
 * program: PerformArithmeticCalculation
 * discription: The programs outputs the result of given cases
 *   1. My Age Substracted from my father's age
 *   2. My birth year multiplied by 2
 *   3. Convert my height in inches to cms
 *   4. Convert my height in inches to feet and inches where inches is an integer
 */
package labs.lab1;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

// Person is the class to illustrate age and birth information, which 
// would be used in Problem 1 and 2.
class Person {
    private int age;
    private LocalDate birth;

    public Person(LocalDate birthday) {
        this.birth = birthday;
        this.age = LocalDate.now().getYear() - birthday.getYear();
    }

    public int substractAge(Person p2) {
        return this.age - p2.age;
    }

    public int doubleBirthYear() {
        return this.birth.getYear() * 2;
    }
}

// Height is used to illustrate heights related operations, like convert, and
// format,
// which would be used in other problems.
class Height {
    private double value;
    private String unit;
    // rule is the convert ratio between those units,
    // so make our code more easy to extend.
    private static Map<String, Double> rule;

    static {
        rule = new HashMap<>();
        rule.put("cm->inch", 0.393701);
        rule.put("cm->feet", 0.0328084);
        rule.put("inch->cm", 2.54);
        rule.put("inch->feet", 0.0833333);
    }

    public Height(double value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public double getValue() {
        return this.value;
    }

    public int getValueAsInt() {
        return (int) this.value;
    }

    public String getUnit() {
        return this.unit;
    }

    public Height convertTo(String unit) {
        if (this.unit.equals(unit)) return this;
        
        String from = this.unit;
        String to = unit;
        return new Height(this.value * rule.get(from + "->" + to), to);
    }

    public String toString() {
        return String.format("%.2f(%s)", this.value, this.unit);
    }
}

public class PerformArithmeticCalculation {
    public static void main(String[] args) {
        Person me = new Person(LocalDate.of(1996, 01, 20));
        Person father = new Person(LocalDate.of(1960, 12, 14));
        System.out.println("The age difference between my father and me:" + father.substractAge(me));
        System.out.println("Double of my birth year:" + me.doubleBirthYear());

        {
            // Use the cm unit as the base Height to output all other unit height.
            Height cmHeight = new Height(171, "cm");
            Height inchHeight = cmHeight.convertTo("inch");
            Height feetHeight = cmHeight.convertTo("feet");
            System.out.println(cmHeight);
            System.out.println(inchHeight);
            System.out.println(feetHeight);
        }

        {
            // Use the inch unit as the base Height to output all other unit height.
            Height inchHeight = new Height(67.3228, "inch");
            Height cmHeight = inchHeight.convertTo("cm");
            Height feetHeight = inchHeight.convertTo("feet");
            System.out.println(inchHeight);
            System.out.println(cmHeight);
            System.out.println(feetHeight);
            System.out.println(cmHeight.getValueAsInt());
            System.out.println(feetHeight.getValueAsInt());
        }
    }
}

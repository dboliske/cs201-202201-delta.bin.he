/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 3
 * date: Mon 31 Jan 9:00 am
 * program: ReadWrite
 * discription: This program shows how do we get a char from user's input.
 */
package labs.lab1;

import java.util.Scanner;

public class GetCharFromInput {
    public static void main(String[] args) {
        System.out.print("Please input your first name: ");
        Scanner scanner = new Scanner(System.in);
        String firstName = scanner.nextLine();
        
        System.out.println("First char: " + firstName.charAt(0));
        scanner.close();
    }
}

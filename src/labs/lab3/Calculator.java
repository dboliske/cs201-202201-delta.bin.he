/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sun 13 Feb 9:00 am
 * program: Calculator
 * description: This program read the grades in a file, and calculate the average 
 * grade.
 */
package labs.lab3;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

class StudentInfo {
    private String name;
    private double grade;

    public StudentInfo(String name, double grade) {
        this.name = name;
        this.grade = grade;
    }

    public double getGrade() {
        return this.grade;
    }
}

class Reader {
    private final String fileName;
    private File file;
    private Scanner scanner;

    public Reader(String fileName) {
        this.fileName = fileName;
    }

    public boolean load() {
        try {
            this.file = new File(this.fileName);
            this.scanner = new Scanner(this.file);
            return true;
        } catch (Exception e) {
            System.out.printf("create file failed:%s", e.toString());
            return false;
        }
    }

    public void close() {
        this.scanner.close();
    }

    public boolean hasNextStudentInfo() {
        return this.scanner.hasNext();
    }

    public StudentInfo nextStudentInfo() {
        String info = this.scanner.nextLine();
        String[] infos = info.split(",");
        StudentInfo si = new StudentInfo(info, Double.parseDouble(infos[1]));
        return si;
    }
}

public class Calculator {
    public static void main(String[] args) {
        Reader r = new Reader("./grades.csv");
        // If load failed, just return silent with logs.
        if (!r.load()) {
            System.out.println("create reader failed");
            return;
        } else {
            ArrayList<StudentInfo> infos = new ArrayList<>();
            while (r.hasNextStudentInfo()) {
                StudentInfo si = r.nextStudentInfo();
                infos.add(si);
            }

            double sum = 0;
            for (int i = 0; i < infos.size(); i++)
                sum += infos.get(i).getGrade();
            double avg = sum / infos.size();
            System.out.printf("average grade: %f \n", avg);
        }
        r.close();
    }
}

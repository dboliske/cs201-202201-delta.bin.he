/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 3
 * date: Sun 13 Feb 9:00 am
 * program: Calculator
 * description: This program read the numbers in a file, and fine the minimum number in it. 
 * grade.
 */
package labs.lab3;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * {72, 101, 108, 108, 111, 32, 101, 118, 101,
 * 114, 121, 111, 110, 101, 33, 32, 76, 111, 111,
 * 107, 32, 97, 116, 32, 116, 104, 101, 115, 101,
 * 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 
 * 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421}
 */
public class FindMin {
    public static void main(String[] args) {
        try  {
            File file = new File("./numbers.csv");
            Scanner scanner = new Scanner(file);

            String numbers = scanner.nextLine();
            String[] numbersSplited = numbers.split(",");

            ArrayList<Integer> data = new ArrayList<>();

            for(int i=0; i<numbersSplited.length; i++) {
                // Use trim to remove the leading and tail whilespace.
                data.add(Integer.parseInt(numbersSplited[i].trim()));
            }

            int min = (int)1E9;

            for(int i=0; i<data.size(); i++) {
                min = Math.min(min, data.get(i));
            }
            System.out.printf("Minium number is: %d \n", min);
        }
        catch (Exception e) {
            System.out.printf("File IO exceptions: %s \n", e.toString());
        }
    }
}

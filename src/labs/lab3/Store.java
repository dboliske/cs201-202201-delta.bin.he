/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 2
 * date: Sun 13 Feb 9:00 am
 * program: Calculator
 * description: This program read the grades from user, and then store them in a file.
 * grade.
 */
package labs.lab3;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Store {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please input your numbers now, input `Done` to stop.");
        ArrayList<Integer> numbers = new ArrayList<>();
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            if (line.equals("Done"))
                break;
            numbers.add(Integer.parseInt(line));
        }
        System.out.println("Please input your file name: ");
        String fileName = scanner.nextLine();
        FileWriter fw;
        try {
            fw = new FileWriter(fileName);
            for (int i = 0; i < numbers.size(); i++) {
                fw.append(numbers.get(i).toString());
                fw.append("\t");
            }
            fw.flush();
            fw.close();
        } catch (Exception e) {
            System.out.printf("write to file failed: %s \n", e.toString());
        }
        scanner.close();
    }
}

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 4
 * date: Mon 24 Jan 9:00 am
 * program: Drawer
 * discription: This program provides ability to show the square on the screen.
 * how does it work: by controlling the width of height of outputed characters, we can eaily construct
 * a square on the screen.
 * size=0, 1, 2 are trival cases, we can hardcode it.
 * size > 2 are non-trival cases, we can use for loop to draw it.
 * It's better to separate the `Pencil` class to another file, but for homework, seems no need.
 * Make it simple.
 * Pseudocode:
 * Filled:
 *  FOR i from 0 ... size
 *      FOR j from 0 ... size
 *          print(character)
 * Non-Filled:
 *  For i from 0 ... size
 *      For j from 0 ... size
 *          if first and last row then print(character)
 *          else if first and last col then print (character)
 */
package labs.lab0;

/**
 * Pencil is used to draw different shape.
 * 
 * @by: set the underlying chareacter presentation.
 * @filled: set if we fill the shape or not.
 */
class Pencil {
    private char by;
    private boolean filled;

    public Pencil(char by, boolean filled) {
        this.by = by;
        this.filled = filled;
    }

    public void drawSquare(int size) {
        if (size <= 0)
            return;

        if (size == 1) {
            System.out.println(Character.toString(this.by));
            return;
        }

        if (size == 2) {
            StringBuffer buf = new StringBuffer();
            buf.append(this.by);
            buf.append(this.by);
            buf.append("\n");
            buf.append(this.by);
            buf.append(this.by);
            System.out.println(buf.toString());
            return;
        }

        StringBuffer buf = new StringBuffer();
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                if (this.filled) {
                    // If filled, every row has same numbers characters.
                    buf.append(this.by);
                } else {
                    // If non-filled, only the first row and last row have all characters.
                    // The others between first and last only have the start and end characters.
                    if (row == 0 || row == size - 1) {
                        buf.append(this.by);
                    } else {
                        if (col == 0 || col == size - 1) {
                            buf.append(this.by);
                        } else {
                            buf.append(" ");
                        }
                    }
                }
            }
            buf.append("\n");
        }
        System.out.println(buf.toString());
    }
}

public class Drawer {
    public static void main(String[] args) {
        Pencil p = new Pencil('*', false);
        System.out.println("test case 1 without filled style: size = 0");
        p.drawSquare(0);
        System.out.println("test case 2 without filled style: size = 1");
        p.drawSquare(1);
        System.out.println("test case 3 without filled style: size = 2");
        p.drawSquare(2);
        System.out.println("test case 4 without filled style: size > 2");
        p.drawSquare(4);

        Pencil p2 = new Pencil('*', true);
        System.out.println("test case 1 with filled style: size = 0");
        p2.drawSquare(0);
        System.out.println("test case 2 with filled style: size = 1");
        p2.drawSquare(1);
        System.out.println("test case 3 with filled style: size = 2");
        p2.drawSquare(2);
        System.out.println("test case 4 with filled style: size > 2");
        p2.drawSquare(4);
    }
}

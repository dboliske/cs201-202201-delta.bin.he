
/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Mon 24 Jan 9:00 am
 * program: HelloWorld
 * description: This program shows a "Hello World!" on a screen.
 */
package labs.lab0;

public class HelloWorld {
    private static String message = "Hello World!";

    public static void main(String[] args) {
        System.out.println(message);
    }
}
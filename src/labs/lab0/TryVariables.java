
/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 2
 * date: Mon 24 Jan 9:00 am
 * program: HelloWorld
 * description: This program shows a "Hello World!" on a screen.
 */
package labs.lab0;

public class TryVariables {

	public static void main(String[] args) {
		// This example shows how to declare and initialize variables

		int integer = 100;
		long large = 42561230L;
		// small = 25;
		// We need type declaration before the name.
		short small = 25;
		byte tiny = 19;

		// float f = .0925F
		// We need semicolon after a declration.
		float f = .0925f;
	    double decimal = 0.725;
		double largeDouble = +6.022E23; // This is in scientific notation

		char character = 'A';
		boolean t = true;

		System.out.println("integer is " + integer);
		System.out.println("large is " + large);
		// system.out.println("small is " + small);
		// The class name is upper case.
		System.out.print("small is " + small);

		// System.out.println("tiny is " + tine);
		// The variable name should be declared already.
		System.out.println("tiny is " + tiny);
		System.out.println("f is " + f);
		// System.out,println("decimal is " + decimal);
		// To access the member we need to use .
		System.out.println("decimal is " + decimal);

		System.out.println("largeDouble is " + largeDouble);
		
		// System.out.println("character is " character);
		// We need  + to concat multiple strings.
		System.out.println("character is " + character);
		System.out.println("t is " + t);
	}

}
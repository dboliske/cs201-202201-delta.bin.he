/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 3
 * date: Mon 24 Jan 9:00 am
 * program: Info
 * discription: This program shows my name and birthday information.
 */
package labs.lab0;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

// Student encapulates name and birthday information.
class Student {
    private String name;
    private LocalDate birthday;

    public Student(String name, int year, int month, int date) {
        this.name = name;
        this.birthday = LocalDate.of(year, month, date);
    }

    @Override
    public String toString() {
        String messageTemplate = "My name is %s and my birthday is %s.";
        return String.format(messageTemplate, this.name, this.birthday.format(DateTimeFormatter.ofPattern("MMM. dd, yyyy")));
    }
}

public class Info {
    public static void main(String[] args) {
        Student hebin = new Student("HEBIN", 1996, 01, 20);
        System.out.print(hebin);
    }
}

# Lab 7

## Total

17/20

## Break Down

- Exercise 1 4/4
- Exercise 2 5/5
- Exercise 3 5/5
- Exercise 4 3/6

## Comments
- In exercise 4, the program didn't allow users to input the search value -1;
  The searching algorithm reported error when the search value didn't represent in the array -2;

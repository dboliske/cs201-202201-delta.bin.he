/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 01, 02, 03
 * date: Sun 3 Apr 9:00 am
 * program: Sortings
 * description: This App shows how do we implement Bubble Sort, Select Sort and Insert Sort.
 */
package labs.lab7;

public class Sortings {
    public static void bubbleSort(int[] nums) {
        int n = nums.length;
        for(int i=0; i<n-1; i++) {
            boolean sorted = true;

            for(int j=0; j<n-i-1;j++) {
                if (nums[j] > nums[j+1]) {
                    int t = nums[j];
                    nums[j] = nums[j+1];
                    nums[j+1] = t;

                    sorted = false;
                }
            }

            if (sorted) break;
        }
    }


    public static void insertSort(String[] strs) {
        int n = strs.length;

        for(int i=1; i<n; i++) {
            String key = strs[i];
            int j = i-1;
            while(j >= 0 && strs[j].compareTo(key) > 0) {
                strs[j+1] = strs[j];
                j --;
            }
            strs[j+1] = key;
        }
    }

    public static void selectSort(double[] nums) {
        int n = nums.length;

        for(int i=0; i<n; i++) {
            int k = i;
            for(int j=i+1; j<n; j++) {
                if (nums[j] < nums[k]) k = j;
            }
            double t = nums[k];
            nums[k] = nums[i];
            nums[i] = t;
        }
    }
}

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 04
 * date: Sun 3 Apr 9:00 am
 * program: Search
 * description: This App shows how do we implement binary search.
 */
package labs.lab7;

public class Search {

    public static int helper(String[] strs, int l, int r, String target) {
        int mid = l + (r - l) / 2;
        if (strs[mid].equals(target))
            return mid;
        else if (strs[mid].compareTo(target) < 0)
            return helper(strs, mid + 1, r, target);
        else if (strs[mid].compareTo(target) > 0)
            return helper(strs, l, mid - 1, target);
        return -1;
    }

    public static int binarySearch(String[] strs, String target) {
        int l = 0;
        int r = strs.length - 1;

        return helper(strs, l, r, target);
    }
}

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Sun 3 Apr 9:00 am
 * program: App
 * description: This App shows how to use those algorithms.
 */
package labs.lab7;

public class App {
    public static void main(String[] args) {
        String[] strs = {"c", "html", "java", "python", "ruby", "scala"};
        int ans = Search.binarySearch(strs, "java");
        if (ans != 2) {
            System.out.println("binarySearch test failed");
        }

        int[] bubbleNums = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
        Sortings.bubbleSort(bubbleNums);
        for(int i=0; i<bubbleNums.length; i++) System.out.print(bubbleNums[i] + " ");
        System.out.println();

        String[] insertStrs = {"cat", "fat", "dog", "apple", "bat", "egg"};
        Sortings.insertSort(insertStrs);
        for(int i=0; i<insertStrs.length; i++) System.out.print(insertStrs[i] + " ");
        System.out.println();

        double[] selectNums = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
        Sortings.selectSort(selectNums);
        for(int i=0; i<selectNums.length; i++) System.out.print(selectNums[i] + " ");
        System.out.println();
    }
}

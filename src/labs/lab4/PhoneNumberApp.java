/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 2
 * date: Sun 20 Feb 9:00 am
 * program: GeoLocation
 * description: This program define the PhoneNumber class and show how to instantiate it.
 * grade.
 */
package labs.lab4;


public class PhoneNumberApp {
    public static void main(String[] args) {
        PhoneNumber p1 = new PhoneNumber();
        PhoneNumber p2 = new PhoneNumber("+65","000", "94571250");

        System.out.println("PhoneNumber: " + p1.toString());
        System.out.println("PhoneNumber: " + p2.toString());
    }
}

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 3
 * date: Sun 20 Feb 9:00 am
 * program: PhoneNumber
 * description: This program define Potion class.
 * grade.
 */
package labs.lab4;

class Potion {
    private String name;
    private double strength;

    public Potion() {
        this.name = "";
        this.strength = 0.0;
    }

    public Potion(String name, double strength) {
        this.name = name;
        this.strength = strength;
    }

    public String getName() {
        return this.name;
    }

    public double getStrength() {
        return this.strength;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    public String toString() {
        return String.format("%s: %.2f", this.name, this.strength);
    }

    public boolean validStrength() {
        return this.strength >= 0 && this.strength <= 10;
    }

    public boolean equals(Potion p2) {
        return this.name.equals(p2.getName()) && this.strength == p2.getStrength();
    }
}
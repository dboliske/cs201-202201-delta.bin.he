/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sun 20 Feb 9:00 am
 * program: GeoLocation
 * description: This program define the GeoLocation and show how to instantiate it.
 * grade.
 */
package labs.lab4;

public class GeoLocationApp {
    public static void main(String[] args) {
        GeoLocation l1 = new GeoLocation();
        GeoLocation l2 = new GeoLocation(12.0, 14.0);

        System.out.println("GeoLocation l1="+l1.toString());
        System.out.println("GeoLocation l2="+l2.toString());
    }
}

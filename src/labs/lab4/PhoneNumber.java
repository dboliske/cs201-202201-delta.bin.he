/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 3
 * date: Sun 20 Feb 9:00 am
 * program: PhoneNumber
 * description: This program define PhoneNumber class.
 * grade.
 */
package labs.lab4;

class PhoneNumber {
    private String countryCode;
    private String areaCode;
    private String number;

    public PhoneNumber() {
        this.countryCode = "";
        this.areaCode = "";
        this.number = "";
    }

    public PhoneNumber(String cCode, String aCode, String number) {
        this.countryCode = cCode;
        this.areaCode = aCode;
        this.number = number;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public String getAreaCode() {
        return this.areaCode;
    }

    public String getNumber() {
        return this.number;
    }

    public void setCountryCode(String cCode) {
        this.countryCode = cCode;
    }

    public void setAreaCode(String aCode) {
        this.areaCode = aCode;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String toString() {
        if (this.countryCode.equals(""))
            return "";
        return String.format("%s-%s-%s", this.countryCode, this.areaCode, this.number);
    }

    public boolean validAreaCode(String aCode) {
        return aCode.length() == 3;
    }

    public boolean validNumber(String number) {
        return number.length() == 7;
    }

    public boolean equals(PhoneNumber p2) {
        return this.areaCode.equals(p2.getAreaCode()) &&
                this.countryCode.equals(p2.getCountryCode()) &&
                this.number.equals(p2.getNumber());
    }
}
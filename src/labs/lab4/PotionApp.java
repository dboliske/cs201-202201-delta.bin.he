/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 3
 * date: Sun 20 Feb 9:00 am
 * program: GeoLocation
 * description: This program define the Potion and show how to instantiate it.
 * grade.
 */
package labs.lab4;

public class PotionApp {
    public static void main(String[] args) {
        Potion p1 = new Potion();
        Potion p2 = new Potion("hebin", 100.0);

        System.out.println("potion: " + p1.toString());
        System.out.println("potion: " + p2.toString());
    }
}

# Final Exam

## Total

84/100

## Break Down

1. Inheritance/Polymorphism:    17/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                3/5
    - Methods:                  4/5
2. Abstract Classes:            20/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                5/5
    - Methods:                  5/5
3. ArrayLists:                  19/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          15/20
    - Compiles:                 5/5
    - Selection Sort:           5/10
    - Results:                  5/5
5. Searching Algorithms:        13/20
    - Compiles:                 5/5
    - Jump Search:              5/10
    - Results:                  3/5

## Comments

1. attributes buidling and roomNumber should be protected -2
   didn't implement hasComputers() method -1
2. ok
3. Error when the first input is 'Done' -1
4. Implement sort using insertion sort instead of selection sort algorithm -5
5. The jump search algorithm has bugs. -5
   When the input value is not present in an array, raise error -2

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: Sorting
 * description: The Sorting implements the insertion sort.
 */
package exams.second.question4;

public class Sorting {
    public static void insertSortAndDisplay(String[] strs) {
        // From the second element
        // we try to find the correct position before,
        // during the find, we would move the larger element to behind one by one if needed.
        for(int i=1; i<strs.length; i++) {
            String pivot = strs[i];
            int j = i - 1;
            while(j >= 0 && strs[j].compareTo(pivot) > 0) {
                strs[j+1] = strs[j];
                j --;
            }
            strs[j+1] = pivot;
        }

        for(String s: strs) {
            System.out.print(s + ",");
        }
    }
}

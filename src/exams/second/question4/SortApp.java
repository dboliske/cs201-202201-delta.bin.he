/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: SortApp
 * description: The SortApp call the insertion sort for the String array, and then output the sorted data.
 */
package exams.second.question4;

public class SortApp {
    public static void main(String[] args) {
        String[] data = new String[]{"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
        Sorting.insertSortAndDisplay(data);
    }
}

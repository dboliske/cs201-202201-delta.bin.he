/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: SearchApp
 * description: The SearchApp call jump search for those data.
 */
package exams.second.question5;

import java.util.Scanner;

public class SearchApp {
    public static void main(String[] args) {
        double[] data = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input what number you want to search");

        while(true) {
            boolean valid = true;

            double value = 0.0;
            try {
                String line = scanner.nextLine();
                value = Double.parseDouble(line);
            } catch (NumberFormatException e) {
                valid = false;
            }
            if (!valid) continue;;

            int ans = Search.jumpSearch(data, value);

            System.out.printf("the number you find is at %d", ans);
            break;
        }

        scanner.close();;
    }
}

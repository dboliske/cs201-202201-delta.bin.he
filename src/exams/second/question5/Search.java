/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: Search
 * description: The Search implements the jump search algorithm.
 */
package exams.second.question5;

public class Search {

    public static int jump(double[] arr, double item, int cur, int step) {
        System.out.println("Cur="+cur + ",step="+step);
        if (cur >= arr.length) return -1;

        if (arr[cur] == item) return cur;
        else if(arr[cur] < item) return jump(arr, item, cur + step, step);
        else return -1;
    }

    public static int jumpSearch(double[] arr, double item)
    {
        int n = arr.length;

        int block = (int)Math.floor(Math.sqrt(n));

        int index = jump(arr, item, 0, block);


        // Doing a linear search for x in block
        // beginning with prev.
        while (index < n && arr[index] < item) index++;

        if (arr[index] == item) return index;

        return -1;
    }
}

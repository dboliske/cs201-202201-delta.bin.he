/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: ArrayListApp
 * description: The ArrayListApp receives the data from user, and the output the max and min value.
 */
package exams.second.question3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ArrayListApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Double> data = new ArrayList<>();

        while(scanner.hasNext()) {
            String line = scanner.nextLine();

            if (line.equals("Done")) break;

            boolean valid = true;
            double value = 0.0;
            try {
                value = Double.parseDouble(line);
            }
            catch (NumberFormatException e) {
                System.out.println("Please input value number");
                valid = false;
            }

            if (valid) data.add(value);
        }

        System.out.println("max value:" + Collections.max(data));
        System.out.println("min value:" + Collections.min(data));

        scanner.close();
    }
}

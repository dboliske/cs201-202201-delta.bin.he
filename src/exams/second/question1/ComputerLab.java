/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: ComputerLab
 * description: The ComputerLab is the derived class for Classroom.
 */
package exams.second.question1;

public class ComputerLab extends Classroom{
    private boolean computers;

    public ComputerLab() {
        super();
    }

    public boolean isComputers() {
        return computers;
    }

    public void setComputers(boolean computers) {
        this.computers = computers;
    }

    @Override
    public String toString() {
        return super.toString() + " => ComputerLab";
    }
}

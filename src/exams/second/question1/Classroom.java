/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: Classroom
 * description: The Classroom is the base class for ComputerLab
 */
package exams.second.question1;

public class Classroom {
    private String building;
    private String roomNumber;
    private int seats;

    public Classroom() {
    }

    public Classroom(String building, String roomNumber, int seats) {
        this.building = building;
        this.roomNumber = roomNumber;
        if (seats < 0) {
            seats = 0;
        }
        this.seats = seats;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        if (seats > 0) {
            this.seats = seats;
        }
    }

    @Override
    public String toString() {
        return String.format("Classroom(building=%s, roomNumber=%s, seats=%d)", building, roomNumber, seats);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Classroom && ((Classroom)obj).getRoomNumber().equals(roomNumber);
    }
}

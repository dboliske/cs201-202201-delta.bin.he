/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: Polygon
 * description: The Polygon is the abstract class for different shapes.
 */
package exams.second.question2;

public abstract class Polygon {
    private String name;

    Polygon() {}

    public Polygon(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract double area();

    public abstract double perimeter();
}

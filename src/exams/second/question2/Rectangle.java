/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: Rectangle
 * description: The Rectangle extends the Polygon abstract method.
 */
package exams.second.question2;

public class Rectangle extends Polygon{
    private double width;
    private double height;

    public Rectangle() {
        super("Rectangle");
    }

    public Rectangle(double width, double height) {
        super("Rectangle");
        if (width < 0) width = 0;
        if (height < 0) height = 0;
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        if (width >= 0) {
            this.width = width;
        }
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        if (height >= 0) {
            this.height = height;
        }
    }

    @Override
    public String toString() {
        return String.format("Rectangle(width=%f,height=%f)", width, height);
    }

    @Override
    public double area() {
        return width * height;
    }

    @Override
    public double perimeter() {
        return 2 * (width + height);
    }
}

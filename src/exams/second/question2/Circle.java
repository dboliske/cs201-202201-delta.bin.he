/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Sat 30 Apr 9:00 am
 * program: Circle
 * description: The Circle extends the Polygon abstract method.
 */
package exams.second.question2;

public class Circle extends Polygon{
    private double radius;

    public Circle() {
        super("Circle");
    }

    public Circle(double radius) {
        super("Circle");
        if (radius < 0) radius = 0;
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        if (radius >= 0) {
            this.radius = radius;
        }
    }

    @Override
    public String toString() {
        return String.format("Circle(radius=%f)", radius);
    }

    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * radius;
    }
}

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 5
 * date: Fir 25 Feb 9:15 pm
 * program: Pet
 * description: This program define the Pet class.
 */
package exams.first;

public class Pet {
    private String name;
    private int age;

    public Pet() {
        this.age = 0;
        this.name = "";
    }
    
    public Pet(String name, int age) {
        this.age = 0;
        this.name = name;

        if (age >= 0) this.age = age;
    }


    public void setName(String name) {
        this.name = name;
    }
    
    public void setAge(int age) {
        if (age >= 0) this.age = age;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public boolean compare(Pet p2) {
        return this.name.equals(p2.getName()) && this.age == p2.getAge();
    }

    public boolean equals(Object obj) {
        if (obj instanceof Pet) {
            return this.compare((Pet)obj);
        }
        return false;
    }

    public String toString() {
        return String.format("name:%s,age:%s ", this.name, this.age);
    }    
}

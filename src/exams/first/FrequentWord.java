/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 4
 * date: Fir 25 Feb 9:15 pm
 * program: FrequentWord
 * description: This program receives some words, and we will print those words that occus more than once.
 */
package exams.first;

import java.util.Scanner;

public class FrequentWord {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input 5 words in order, we will print the word that occurs more than once.");

        String[] words = new String[5];
        for(int i=0; i<5; i++) {
            words[i] = scanner.nextLine();
        }

        for(int i=0; i<5; i++) {
            boolean duplicated = false;
            // Find all the duplicated with i-index word
            // And then we replace it with "" to avoid multiple print.
            for(int j=i+1; j<5; j++) {
                if (words[i].equals(words[j])) {
                    if (!duplicated) {
                        System.out.println(words[j]);
                        duplicated = true;
                    }
                    words[j] = "";
                }
            }
        }

        scanner.close();
    }
}

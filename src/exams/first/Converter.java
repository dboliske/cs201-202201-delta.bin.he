/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 1
 * date: Fir 25 Feb 9:15 pm
 * program: Converter
 * description: This program receives a integer, and convert it to a char with adding 65.
 */
package exams.first;

import java.util.Scanner;

public class Converter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Please input a integer: ");

        int value = scanner.nextInt();
        System.out.println("The converted character is:" + (char)(value + 65));

        scanner.close();
    }
}

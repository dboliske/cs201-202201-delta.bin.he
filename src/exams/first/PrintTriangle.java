/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 3
 * date: Fir 25 Feb 9:15 pm
 * program: PrintTriangle
 * description: This program uses repetition to draw Triangle.
 */
package exams.first;

import java.util.Scanner;

public class PrintTriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); 
        System.out.println("Please input the triangle's witdh and height: ");

        int width = scanner.nextInt();
        for(int i=0; i<width; i++) {
            // Repeately print the whitespace for alignment
            for(int j=0; j<i; j++) System.out.print(" ");
            // Repately print the wildstar for triangle.
            for(int j=0; j<width-i; j++) System.out.print("*");
            System.out.println();
        }

        scanner.close();
    }
}

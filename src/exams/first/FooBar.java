/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: 2
 * date: Fir 25 Feb 9:15 pm
 * program: FooBar
 * description: This program receives a integer, and print different messages based on its value.
 */
package exams.first;

import java.util.Scanner;

public class FooBar {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please input an integer: ");
        
        int value = scanner.nextInt();

        if (value % 6 == 0) {
            System.out.println("foobar");
        }
        else if (value % 2 == 0) {
            System.out.println("foo");
        }
        else if (value % 3 == 0) {
            System.out.println("bar");
        }

        scanner.close();
    }
}

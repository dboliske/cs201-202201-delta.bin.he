# Shop System Desgin
Basically our system module definitions should be like as below.
![](./Requirements.png)
- Shop: illustrate the Shop directly
- Seller: illustrate the Shop's administrator directly
- Buyer: illustrate the customers for Shopping directly
- Item: illustrate all items in a Shop, the Buyer can buy those items, and the Seller could manage items in Shop

In general, our system could contains two parts, one is management, another is the shopping part.
So our options could separate to two parts, and we provide n option for users to decide which part it is.

## Menu Options
> User Interface 
- Seller or Buyer? 
    - Seller
        - Add Item to Shop
        - Delete Item in Shop
        - Update Item in Shop
    - Buyer
        - Add Item to Cart
        - Delete Item in Cart
        - Update Item in Cart
        - Checkout

## Programmer's Tasks
    1. Use `Scanner` and `File` to read the given file
    2. Read each line of the file, and parse them to different component. (name, price, date)
    3. We can use ArrayList to store those data, (Item list)
    4. Call the builtin methods of ArrayList to do add/delete data, if we need to modify the data, we just update the Item directly.
    5. We sort the Items based on the name in order, and then run Binary Search to search the items
    6. `User`, `Seller`, `Buyer`, `Shop`, `Cart`, `Order`, `Item`, `ProduceItem`, `ShelvedItem`, `AgeRestrictedItem`

## UML
Our UML is defined here as below.
![](./Entity%20UML.png)


## Test Plans
Refer to the Menu Options:

- Seller or Buyer?              1
    - Seller                    
        - Add Item to Shop      3
        - Delete Item in Shop   4
        - Update Item in Shop   5
    - Buyer                     
        - Add Item to Cart      7
        - Delete Item in Cart   8
        - Update Item in Cart   9
        - Checkout              10

Test Case 1: 
Description: Choose Role
Input: Seller or Buyer
Expectation: It should show Seller's menu option, or Buyer's menu option based on the input. The invalid Input should be ignored, and print the main menu option again.

Test Case 3:
Description: Add Item to Shop
Input: Item information
Expectation: the Item should be stored in the Shop, which can be view later then. The invalid Item information would trigger a warnning, and ask for re-input.

Test Case 4:
Description: Delete Item in Shop
Input: an existing Item
Expectation: the Item should be deleted from the Shop, which can be confirmed when we view Shop. The invalid Item could be ignored, and show warning information for Sellers.

Test Case 5:
Description: Update Item in Shop
Input: an Item with existing Item ID
Expectation: the Item wit the same ID should be replaced by the new Item, which can be confirmed when we view Shop. A Non-Existing Item would be discarded, and print warnning information to users.

Test Case 7:
Description: Add Item to Cart
Input: an Item in the Shop
Expectation: When we choose an Item and add to the Cart, which would reflects to our Cart, we can check our Cart for confirmation. And the number of Items should be no more than the number in stock.

Test Case 8:
Description: Delete Item in Cart
Input: an Item in the Cart
Expectation: When we delete an item in the Cart, the number should be changed. if the number for that Item is 0, we should delete the Item as well.

Test Case 9: 
Description: Update Item in Cart
Input: an Item in the Cart
Expectation: When we update an item in the Cart, it must be existing in the Cart, and it will replace the existing Item data.
Invalid new Item would be discarded with warnning information

Test Case 10:
Description: Checkout
Inout: N/A
Expectation: We will try to checkout with the items in the Cart, when we checkout, it must have enough resource in the Shop, and we will generate an Order Information for our checkout.
/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: Shop class
 * description: Shop is used to represent an implementation for IShop.
 */
package project.shop;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class Shop implements IShop{
    final static String ADD_ITEM_TO_SHOP = "add item";
    final static String DEL_ITEM_IN_SHOP = "del item";
    final static String UPDATE_ITEM_IN_SHOP = "update item";

    final static String SAVE_CHANGE_IN_SHOP = "save";

    final static String LIST_IN_SHOP = "list";

    final static String EXIT = "exit";

    final static String ADD_ITEM_TO_CART = "add item";
    final static String DEL_ITEM_IN_CART = "del item";
    final static String UPDATE_ITEM_IN_CART = "update item";

    final static String CHECKOUT_IN_CART = "checkout";

    final static String PRODUCED_ITEM = "produced item";
    final static String AGE_RESTRICTED_ITEM = "age restricted item";
    final static String SHELVED_ITEM = "shelved item";

    private String name;
    private int sellerId;
    private String address;
    private String postcode;
    private int ownerId;
    private ArrayList<Item> stock;

    private String dbname;

    public Shop() {
        this.stock = new ArrayList<>();
    }

    public Shop(String name, String address, String postcode, int ownerId, String dbname) {
        this.name = name;
        this.address = address;
        this.postcode = postcode;
        this.ownerId = ownerId;
        this.stock = new ArrayList<>();
        this.dbname = dbname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public void setStock(ArrayList<Item> stock) {
        this.stock = stock;
    }

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public ArrayList<Item> getStock() {
        return stock;
    }

    @Override
    public void loadStock() {
        Scanner itemLoader;
        try {
            itemLoader = new Scanner(new File(dbname));
        }
        catch (IOException e) {
            System.out.println("read stock file error:" + e.toString());
            return;
        }
        while(itemLoader.hasNext()) {
            String line = itemLoader.nextLine();
            addItemToStock(parseItem(line));
        }
        itemLoader.close();
    }

    @Override
    public void dumpStock() {
        try {
            FileWriter fw = new FileWriter(dbname + "_" + LocalTime.now().toString());
            for (Item item : this.stock) {
                fw.append(item.toString()).append("\n");
            }
            fw.flush();
            fw.close();
        } catch (IOException e) {
            System.out.println("refresh stock to db failed:" + e.toString());
        }
    }

    public void displayItems() {
        for (Item item : this.stock) {
            System.out.println(item);
        }
    }

    public void displayItemsWithAgeRestriction(int age) {
        for(Item item: this.stock) {
            if (item instanceof AgeRestrictedItem) {
                if (((AgeRestrictedItem) item).getAgeRestriction() < age) System.out.println(item);
            }
        }
    }

    private Item parseItem(String itemInfo) {
        String[] infos = itemInfo.split(",");
        String name = infos[0];
        double price = Double.parseDouble(infos[1]);

        if (infos.length == 2) {
            return new ShelvedItem(name, price);
        }
        else {
            if (infos[2].contains("/")) {
                return new ProduceItem(name, price, LocalDate.parse(infos[2], DateTimeFormatter.ofPattern("MM/dd/yyyy")));
            }
            else {
                int age = Integer.parseInt(infos[2]);
                return new AgeRestrictedItem(name, price, age);
            }
        }
    }

    public void addItemToStock(Item item) {
        boolean alreadyHas = false;
        for (Item value : this.stock) {
            if (value.getName().equals(item.getName())) {
                int count = value.getCount();
                value.setCount(count + 1);
                alreadyHas = true;
                break;
            }
        }
        if (!alreadyHas) {
            this.stock.add(item);
        }
    }

    public void updateItem(Item item) {
        for(int i=0; i<this.stock.size(); i++) {
            if (this.stock.get(i).getName().equals(item.getName())) {
                this.stock.set(i, item);
            }
        }
    }

    public void deleteItem(Item item) {
        for(int i=0; i<this.stock.size(); i++) {
            if (this.stock.get(i).getName().equals(item.getName())) {
                this.stock.remove(this.stock.get(i));
            }
        }
    }

    public void deleteItemByName(String name) {
        stock.removeIf(item -> item.getName().equals(name));
    }

    public Item findItemByNameInStock(String name) {
        for (Item item : stock) {
            if (item.getName().equals(name)) return item;
        }
        return new Item();
    }


    private void displaySellerOptions() {
        System.out.println("************************************");
        System.out.println("hi Seller, please input your command");
        System.out.println("************************************");
        System.out.println("- " + ADD_ITEM_TO_SHOP);
        System.out.println("- " + DEL_ITEM_IN_SHOP);
        System.out.println("- " + UPDATE_ITEM_IN_SHOP);
        System.out.println("- " + SAVE_CHANGE_IN_SHOP);
        System.out.println("- " + LIST_IN_SHOP);
        System.out.println("- " + EXIT);
        System.out.println("************************************");
    }

    private void displaySupportedItemTypes() {
        System.out.println("************************************");
        System.out.println("Please input item type:");
        System.out.println("- " + PRODUCED_ITEM);
        System.out.println("- " + SHELVED_ITEM);
        System.out.println("- " + AGE_RESTRICTED_ITEM);
        System.out.println("- " + EXIT);
        System.out.println("************************************");
    }

    private void addShelvedItemToStock(String name, double price) {
        addItemToStock(new ShelvedItem(name, price));
        System.out.println("Add " + SHELVED_ITEM + " Successfully");
    }

    private void addProducedItemToStock(String name, double price, LocalDate date) {
        addItemToStock(new ProduceItem(name, price, date));
        System.out.println("Add " + PRODUCED_ITEM + " Successfully");
    }

    private void addAgeRestrictedToStock(String name, double price, int age) {
        addItemToStock(new AgeRestrictedItem(name, price, age));
        System.out.println("Add " + AGE_RESTRICTED_ITEM + " Successfully");
    }
    private void serveSeller(Scanner commandLoader) {
        while(true) {
            displaySellerOptions();

            String command = commandLoader.nextLine();
            switch (command) {
                case EXIT: return;
                case LIST_IN_SHOP: {
                    displayItems();
                    break;
                }
                case ADD_ITEM_TO_SHOP: {
                    displaySupportedItemTypes();

                    String type = commandLoader.nextLine();
                    if (type.equals(EXIT)) break;

                    System.out.println("Please input item name:");
                    String name = commandLoader.nextLine();

                    System.out.println("Please input item price:");
                    double price = Double.parseDouble(commandLoader.nextLine());

                    switch (type) {
                        case SHELVED_ITEM:
                            addShelvedItemToStock(name, price);
                            break;
                        case PRODUCED_ITEM:
                            System.out.println("Please input best before: MM/dd/YYYY");

                            String dateStr = commandLoader.nextLine();
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
                            LocalDate date = LocalDate.parse(dateStr, formatter);

                            addProducedItemToStock(name, price, date);
                            break;
                        case AGE_RESTRICTED_ITEM:
                            System.out.println("Please input legal age:");

                            int ageRestriction = commandLoader.nextInt();

                            addAgeRestrictedToStock(name, price, ageRestriction);
                            break;
                    }
                    break;
                }
                case DEL_ITEM_IN_SHOP: {
                    System.out.println("************************");
                    System.out.println("Please input what item do you want to delete:");
                    displayItems();
                    System.out.println("************************");

                    String name = commandLoader.nextLine();
                    deleteItemByName(name);
                    displayItems();
                    break;
                }
                case UPDATE_ITEM_IN_SHOP: {
                    System.out.println("************************");
                    System.out.println("Please input what item do you want to delete:");
                    displayItems();
                    System.out.println("************************");

                    String name = commandLoader.nextLine();
                    Item item = findItemByNameInStock(name);
                    if (item.getName().equals(name)) {
                        System.out.println("Please confirm you want to update this item:");
                        System.out.println(item);
                    }

                    if (item instanceof ShelvedItem) {
                        System.out.println("Please input the new price for this item:");

                        double newPrice = Double.parseDouble(commandLoader.nextLine());
                        ((ShelvedItem)item).setPrice(newPrice);

                        updateItem(item);

                        displayItems();
                    } else if (item instanceof ProduceItem) {
                        System.out.println("Please input the new price for this item:");

                        double newPrice = Double.parseDouble(commandLoader.nextLine());
                        item.setPrice(newPrice);

                        System.out.println("Please input the new date for this item:");
                        String dateStr = commandLoader.nextLine();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
                        LocalDate date = LocalDate.parse(dateStr, formatter);

                        ((ProduceItem) item).setExpireDate(date);

                        updateItem(item);

                        displayItems();
                    } else if (item instanceof AgeRestrictedItem) {
                        System.out.println("Please input the new price for this item:");

                        double newPrice = Double.parseDouble(commandLoader.nextLine());
                        item.setPrice(newPrice);

                        System.out.println("Please input the new age for this item:");
                        int ageRestrict = Integer.parseInt(commandLoader.nextLine());

                        ((AgeRestrictedItem) item).setAgeRestriction(ageRestrict);

                        updateItem(item);

                        displayItems();
                    }
                    break;
                }
                case SAVE_CHANGE_IN_SHOP: {
                    dumpStock();
                    break;
                }
            }
        }
    }

    private Customer onNewCustomer(Scanner commandLoader) {
        Customer customer = new Customer();
        System.out.println("hi Customer, please input your profile");
        System.out.println("Please input your name");
        String name = commandLoader.nextLine();
        System.out.println("Please input your age");
        int age = Integer.parseInt(commandLoader.nextLine());
        customer.setName(name);
        customer.setAge(age);
        return customer;
    }

    private void displayCart(Customer customer) {
        System.out.println("************************************");
        System.out.println("hi Customer, your cart information: ");
        customer.displayCarts();
        System.out.println("************************************");
    }

    private void displayItemsForCustomerToBuy(Customer customer) {
        System.out.println("************************************");
        System.out.println("hi Customer, you can buy those items:");
        displayItemsWithAgeRestriction(customer.getAge());
        System.out.println("************************************");
    }

    private void displayOptionsForCustomer() {
        System.out.println("************************************");
        System.out.println("hi Customer, please input your command");
        System.out.println("- " + ADD_ITEM_TO_CART);
        System.out.println("- " + DEL_ITEM_IN_CART);
        System.out.println("- " + UPDATE_ITEM_IN_CART);
        System.out.println("- " + CHECKOUT_IN_CART);
        System.out.println("- " + EXIT);
        System.out.println("************************************");
    }

    private void decreaseCountForItemByItemName(String name, int decrease) {
        for(Item item: stock) {
            if (item.getName().equals(name)) {
                if (item.getCount() <= decrease) decrease = item.getCount();
                item.setCount(item.getCount() - decrease);
                if (item.getCount() == 0) stock.remove(item);
            }
        }
    }


    private String checkItemType(String name) {
        for(Item item: stock) {
            if(item.getName().equals(name)) {
                if (item instanceof  AgeRestrictedItem) return AGE_RESTRICTED_ITEM;
                else if (item instanceof  ProduceItem) return PRODUCED_ITEM;
            }
        }

        return SHELVED_ITEM;
    }

    private void serveCustomer(Scanner commandLoader) {
        Customer customer = onNewCustomer(commandLoader);

        displayCart(customer);

        while(true) {
            displayOptionsForCustomer();
            String command = commandLoader.nextLine();

            switch (command) {
                case EXIT: return;
                case ADD_ITEM_TO_CART: {
                    System.out.println("Please input what you want to buy");
                    displayItemsForCustomerToBuy(customer);
                    String itemName = commandLoader.nextLine();

                    System.out.println("Please input how many you want to buy");
                    int itemCount = Integer.parseInt(commandLoader.nextLine());
                    String type = checkItemType(itemName);
                    Item itemInStock = findItemByNameInStock(itemName);

                    switch (type) {
                        case SHELVED_ITEM: customer.addItem(new ShelvedItem(itemName, itemCount));
                        case PRODUCED_ITEM: customer.addItem(new ProduceItem(itemName, itemCount, ((ProduceItem)itemInStock).getExpireDate()));
                        case AGE_RESTRICTED_ITEM: customer.addItem(new AgeRestrictedItem(itemName, itemCount, ((AgeRestrictedItem)itemInStock).getAgeRestriction()));
                    }
                    decreaseCountForItemByItemName(itemName, itemCount);
                    break;
                }
                case DEL_ITEM_IN_CART: {
                    System.out.println("Please input what you want to delete");
                    String itemName = commandLoader.nextLine();
                    Item itemInCart = customer.lookupItemByName(itemName);
                    addItemToStock(itemInCart);
                    customer.decreaseItemInCartByItemName(itemName, itemInCart.getCount());
                    break;
                }
                case UPDATE_ITEM_IN_CART: {
                    System.out.println("Please input what you want to update");
                    String itemName = commandLoader.nextLine();
                    Item item = customer.lookupItemByName(itemName);

                    System.out.println("Please input how many you want to update");
                    int newCount = Integer.parseInt(commandLoader.nextLine());
                    item.setCount(newCount);
                    break;
                }
                case CHECKOUT_IN_CART: {
                    dumpStock();
                }
            }
        }
    }

    @Override
    public void serve() {
        loadStock();
        Scanner commandLoader = new Scanner(System.in);
        String role = "";
        while(true) {
            System.out.printf("welcome to %s, we have those items in stock:%n", getName());
            System.out.println("************************************");
            displayItems();
            System.out.println("************************************");
            System.out.println("Please input your role: seller or customer");

            role = commandLoader.nextLine();

            switch (role) {
                case EXIT: return;
                case Role.SELLER: {
                    serveSeller(commandLoader);
                    break;
                }
                case Role.CUSTOMER: {
                    serveCustomer(commandLoader);
                    break;
                }
                default: {
                    System.out.println("Please input valid role, `seller` or `customer`");
                    break;
                }
            }
        }
    }
}

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: Seller class
 * description: Seller is used to represent the user that manage the Shop.
 */
package project.shop;

public class Seller extends User {
    private int shopId;
    private int exp;
    private Shop shop;

    public Seller() {
        super();
    }

    public Seller(int shopId, int exp){
        super();
        this.shopId = shopId;
        this.exp = exp;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public void addItem(Item item) {
        this.shop.addItemToStock(item);
    }

    public void updateItem(Item item) {
        this.shop.updateItem(item);
    }

    public void deleteItem(Item item) {
        this.shop.deleteItem(item);
    }
}

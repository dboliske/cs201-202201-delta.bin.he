/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: Role class
 * description: Role define two kinds of user in the system.
 */
package project.shop;

public class Role {
    public static final String SELLER = "seller";
    public static final String CUSTOMER = "customer";
}


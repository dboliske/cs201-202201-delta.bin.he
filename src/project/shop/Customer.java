/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: Customer class
 * description: Customer is used to represent the user that want to buy items.
 */
package project.shop;

import java.util.ArrayList;

public class Customer extends User{
    private String profile;
    private final ArrayList<Item> cart;

    public Customer() {
        super();
        cart = new ArrayList<>();
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public void addItem(Item item) {
        this.cart.add(item);
    }

    public void updateItem(Item item) {
        for(int i=0; i<this.cart.size(); i++) {
            if (this.cart.get(i).getName().equals(item.getName())) {
                this.cart.set(i, item);
            }
        }
    }

    public void deleteItem(Item item) {
        for(int i=0; i<this.cart.size(); i++) {
            if (this.cart.get(i).getName().equals(item.getName())) {
                this.cart.remove(this.cart.get(i));
            }
        }
    }

    public String checkout() {
        return String.format("Order:\n", this.cart.size());
    }

    public void displayCarts() {
        for(Item item: this.cart) {
            System.out.println(item);
        }
    }


    public Item lookupItemByName(String name) {
        for(Item item: cart) {
            if (item.getName().equals(name)) return item;
        }
        return new Item();
    }
    public void decreaseItemInCartByItemName(String name, int decrease) {
        for(Item item: cart) {
            if (item.getName().equals(name)) {
                if (item.getCount() <= decrease) decrease = item.getCount();
                item.setCount(item.getCount() - decrease);
                if (item.getCount() == 0) cart.remove(item);
            }
        }
    }
}

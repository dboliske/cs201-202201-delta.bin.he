/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: ShelvedItem class
 * description: ShelvedItem is used to represent the item that in Shelved.
 */
package project.shop;

public class ShelvedItem extends Item{
    public ShelvedItem() {
        super();
    }

    public ShelvedItem(String name, double price) {
        super(name, price, 1, "shelved item");
    }

    @Override
    public String toString() {
        return super.toString();
    }
}


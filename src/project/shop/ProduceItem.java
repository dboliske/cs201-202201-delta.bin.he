/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: ProduceItem class
 * description: ProduceItem is used to represent the item that has time limitation.
 */
package project.shop;

import java.time.LocalDate;

public class ProduceItem extends Item {
    private LocalDate expireDate;

    public ProduceItem() {
        super();
    }

    public ProduceItem(String name, double price, LocalDate date) {
        super(name, price, 1, "produced item");
        this.expireDate = date;
    };

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("best before: %s", this.expireDate.toString());
    }
}

/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: User class
 * description: User is used to represent the basic common properties for all Roles.
 */
package project.shop;

public class User {
    private String name;
    private int age;
    private int gender;
    private int role;
    private String id;

    public User() {}

    public User(String name, int age, int gender, int role, String id) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.role = role;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format("User(%s, %d, %d, %d, %s)",
         this.name, this.age, this.gender, this.role, this.id);
    }
}

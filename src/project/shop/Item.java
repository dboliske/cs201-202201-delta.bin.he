/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: Item class
 * description: Item is used to represent every kind of Item, the common proprties.
 */
package project.shop;

import java.util.ArrayList;

public class Item {
    private String name;
    private double price;
    private int count;
    private ArrayList<Integer> categories;
    private String description;

    public Item() {
        this.categories = new ArrayList<>();
    }

    public Item(String name, double price, int count, String description) {
        this.name = name;
        this.price = price;
        this.count = count;
        this.categories = new ArrayList<>();
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<Integer> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Integer> categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("name=%s, price=%f, count=%d, description=%s",this.name, this.price, this.count, this.description);
    }
}

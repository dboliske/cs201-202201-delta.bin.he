/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: AgeRestrictedItem class
 * description: AgeRestrictedItem is used to represent the item that has age limitation.
 */
package project.shop;

public class AgeRestrictedItem extends Item {
    private int ageRestriction;

    public AgeRestrictedItem() {
        super();
    }

    public AgeRestrictedItem(String name, double price,int ageRestriction) {
        super(name, price, 1, "age restricted item");
        this.ageRestriction = ageRestriction;
    }

    public int getAgeRestriction() {
        return ageRestriction;
    };

    public void setAgeRestriction(int ageRestriction) {
        this.ageRestriction = ageRestriction;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("age more than:%d", this.ageRestriction);
    }
}


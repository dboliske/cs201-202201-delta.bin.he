/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: IShop class
 * description: IShop is used to represent a Shop illustration.
 */
package project.shop;

public interface IShop {
    void loadStock();

    void dumpStock();

    void serve();
}
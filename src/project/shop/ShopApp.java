/**
 * name: HE BIN (delta.bin.he@gmail.com)
 * course: cs-201
 * section: 03
 * problem: N/A
 * date: Thu 28 Apr 9:00 am
 * program: ShopApp class
 * description: ShopApp is used to start the Shop system.
 */
package project.shop;

public class ShopApp {
    final static String shopName = "panda shop";
    final static String shopAddress = "panda zoo";
    final static int shopOwnerId = 0;
    final static String shopPostcode = "11111";
    final static String shopDB = "/Users/binhe/cs201-202201-delta.bin.he/src/project/stock.csv";

    public static void main(String[] args) {
        Shop shop = new Shop(shopName, shopAddress,shopPostcode,shopOwnerId, shopDB);
        shop.serve();
    }
}
